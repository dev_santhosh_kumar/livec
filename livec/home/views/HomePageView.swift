//
//  HomePageView.swift
//
//  Created by Santhosh on 21/12/19.
//  Copyright © 2019 Livec. All rights reserved.
//
import UIKit

class HomePageView: UIView {
    
    private var tableViewController: HomePageItemController!
    
    init(container: UIView) {
        
        super.init(frame: container.frame)
        
        //Initialize properties
        tableViewController = HomePageItemController()
        
        VuUtil.encloseInContainer(container: container, view: self)
        let gradientLayer = ViewFactory.getDefaultGradientLayer(frame: self.bounds)
        self.layer.insertSublayer(gradientLayer, at: 0)
        
        self.addSubview(tableViewController.view)
        
    }
    
    func getTableViewController() -> HomePageItemController {
        return tableViewController
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
