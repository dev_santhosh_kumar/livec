//
//  HomePageItemController.swift
//
//  Created by Santhosh on 22/12/19.
//  Copyright © 2019 Livec. All rights reserved.

import UIKit

class HomePageItemController: UITableViewController {
    private var itemStore: HomePageItemStore!
    
    //Contracts for UITableViewController
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemStore.getItems().count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "UITableViewCell")
        let item = itemStore.getItems()[indexPath.row]
        cell.textLabel?.text = item.getName()
        return cell
    }
    
    func getItemStore() -> HomePageItemStore {
        return itemStore
    }
    
    func setItemStore(itemStore: HomePageItemStore) {
        self.itemStore = itemStore
    }
    
}

