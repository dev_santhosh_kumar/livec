//
//  HomePageItemStore.swift
//
//  Created by Santhosh on 22/12/19.
//  Copyright © 2019 Livec. All rights reserved.

import UIKit

class HomePageItemStore {
    private var items = [HomePageItem]()
    
    init() {
        createItems()
    }
    
    private func createItems(){
        self.items.append(HomePageItem(name: "Rooms"))
        self.items.append(HomePageItem(name: "Attendance"))
    }
    
    func getItems() -> [HomePageItem] {
        return items
    }
}
