//  LoginViewController.swift
//
//  Created by Santhosh on 22/07/19.
//  Copyright © 2019 Livec. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    private var baseView: BaseView!
    private var homePageView: HomePageView!
    
    override func loadView() {
        super.loadView()
        baseView = ViewFactory.getBaseView(container: self.view)
        homePageView = HomePageView(container: baseView.getMainView())
        
        let itemStore = HomePageItemStore()
        homePageView.getTableViewController().setItemStore(itemStore: itemStore)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}
