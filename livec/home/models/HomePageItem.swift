//
//  HomePageItem.swift
//
//  Created by Santhosh on 22/12/19.
//  Copyright © 2019 Livec. All rights reserved.

import UIKit

class HomePageItem: NSObject {
    private let name: String!
    
    init(name: String) {
        self.name = name
        super.init()
    }
    
    func getName() -> String {
        return self.name
    }
}
