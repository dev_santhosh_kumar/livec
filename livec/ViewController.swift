//  LoginViewController.swift
//
//  Created by Santhosh on 22/07/19.
//  Copyright © 2019 Livec. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private var baseView: BaseView!
    private var loginView: LoginView!
    
    override func loadView() {
        super.loadView()
        baseView = ViewFactory.getBaseView(container: self.view)
        loginView = LoginView(container: baseView.getMainView(), controller: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(loginView != nil) {
            loginView!.getEmail().becomeFirstResponder()
        }
    }
    
    func signIn(){
//        LoginService().login(userName: loginView.getEmail().text!, password: loginView.getPassword().text!, viewController: self)
        let homeViewController = HomeViewController();
        present(homeViewController, animated: false, completion: nil)
    }
}
