//
//  LoginView.swift
//  Created by Santhosh on 29/07/19.
//  Copyright © 2019 Livec. All rights reserved.
//

import UIKit

class LoginView: UIView {
    private var email: TextField!
    private var password: TextField!
    private var signInBtn: Button!
    private var registerBtn: Button!
    private let controller: ViewController!
    
    //TODO : Internationalize
    private let emailPlaceholder = "Email"
    private let passwordPlaceholder = "Password"
    private let signIn = "Sign In"
    private let register = "Register"
    
    
    init(container: UIView, controller: ViewController) {
        self.controller = controller
        super.init(frame: container.frame)
        
        //Initialize properties
        email = TextField(frame: CGRect.null, placeHolder: emailPlaceholder, keyboardType:                         UIKeyboardType.emailAddress);
        password = TextField(frame: CGRect.null, placeHolder: passwordPlaceholder, keyboardType:
            UIKeyboardType.default);
        signInBtn = Button(frame: CGRect.null, title: signIn)
        registerBtn = Button(frame: CGRect.null, title: register)
        
        VuUtil.encloseInContainer(container: container, view: self)
        let gradientLayer = ViewFactory.getDefaultGradientLayer(frame: self.bounds)
        self.layer.insertSublayer(gradientLayer, at: 0)
        
        self.addSubview(email)
        self.addSubview(password)
        self.addSubview(signInBtn)
        self.addSubview(registerBtn)
        
        configureEmail()
        configurePassword()
        configureSignInBtn()
        configureRegisterBtn()
    }
    
    private func configureEmail() {
        //Add Constraints
        email.translatesAutoresizingMaskIntoConstraints = false
        email.topAnchor.constraint(equalTo: self.topAnchor, constant: self.frame.height/3 ).isActive = true
        email.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: self.frame.width/10).isActive = true
        email.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.8).isActive = true
        email.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.05).isActive = true
    }
    
    private func configurePassword() {
        password.isSecureTextEntry = true
        //Add Constraints
        password.translatesAutoresizingMaskIntoConstraints = false
        password.topAnchor.constraint(equalTo: self.topAnchor, constant: (self.frame.height/3) + (self.frame.height * 0.06) ).isActive = true;
        password.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: self.frame.width/10).isActive = true;
        password.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.8).isActive = true
        password.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.05).isActive = true
    }
    
    private func configureSignInBtn() {
        //Add Constraints
        signInBtn.translatesAutoresizingMaskIntoConstraints = false
        signInBtn.topAnchor.constraint(equalTo: self.topAnchor, constant: (self.frame.height/3) + (2 * (self.frame.height * 0.06))).isActive = true;
        signInBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: self.frame.width/10).isActive = true;
        signInBtn.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4).isActive = true
        signInBtn.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.05).isActive = true
        
        //Event Listeners
        signInBtn.addTarget(self, action: #selector(btnClicked), for: UIControl.Event.touchUpInside)
    }
    
    private func configureRegisterBtn() {
        //Add Constraints
        registerBtn.translatesAutoresizingMaskIntoConstraints = false
        registerBtn.topAnchor.constraint(equalTo: self.topAnchor, constant: (self.frame.height/3) + (2 * (self.frame.height * 0.06))).isActive = true;
        registerBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: self.frame.width * 0.5).isActive = true;
        registerBtn.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4).isActive = true
        registerBtn.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.05).isActive = true
        
        //Event Listeners
        registerBtn.addTarget(self, action: #selector(btnClicked), for: UIControl.Event.touchUpInside)
    }
   
    @objc func btnClicked(_ sender: UIButton) {
        let colorAnimation = ViewFactory.getButtonClickAnimation()
        sender.layer.add(colorAnimation, forKey: "ColorPulse")
        if(sender == signInBtn) {
            controller?.signIn() 
        }
       
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.email.resignFirstResponder()
        self.password.resignFirstResponder()
    }
    
    func getEmail() -> TextField {
        return email;
    }
    
    func getPassword() -> TextField {
        return password
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
