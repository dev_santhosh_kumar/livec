//
//  ColorConstants.swift
//  livec
//
//  Created by Livec on 03/08/19.
//  Copyright © 2019 Livec. All rights reserved.
//

import Foundation

class ColorConstants {
   
    public static let titleBackground = "#00aaff"
    public static let commonGradientTop = "#00aaff"
    public static let commonGradientBottom = "#000000"
    public static let textFieldBackground = "#ffffff"
    public static let buttonBackground = "#e3e3e3"
    public static let buttonTitleColor = "#000000"
    public static let buttonPressAnimColor = "#00aaff"
    
}
