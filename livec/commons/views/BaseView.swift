//
//  BaseView.swift
//
//  Created by Santhosh on 28/07/19.
//  Copyright © 2019 Livec. All rights reserved.
//

import UIKit

class BaseView: UIView {
    
    private var titleView: TitleView!
    private var mainView: MainView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func getTitleView() -> TitleView {
      return self.titleView
    }
    
    func setTitleView(titleView: TitleView) {
        self.titleView = titleView
    }
    
    func getMainView() -> MainView {
        return self.mainView
    }
    
    func setMainView(mainView: MainView) {
        self.mainView = mainView
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
}
