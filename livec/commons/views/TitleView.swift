//
//  TitleView.swift
//
//  Created by Santhosh on 28/07/19.
//  Copyright © 2019 Livec. All rights reserved.
//

import UIKit

class TitleView: UIView {
    
    private var leadingView: TitleViewLeadingView!
    private var centerView: TitleViewCenterView!
    private var trailingView: TitleViewTrailingView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    func setLeadingView(leadingView: TitleViewLeadingView) {
        self.leadingView = leadingView;
    }
    
    func getLeadingView() -> TitleViewLeadingView {
        return self.leadingView
    }
    
    func setCenterView(centerView: TitleViewCenterView) {
        self.centerView = centerView
    }
    
    func getCenterView() -> TitleViewCenterView {
        return self.centerView
    }
    
    func setTrailingView(trailingView: TitleViewTrailingView) {
        self.trailingView = trailingView
    }
    
    func getTrailingView() -> TitleViewTrailingView {
        return self.trailingView
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
