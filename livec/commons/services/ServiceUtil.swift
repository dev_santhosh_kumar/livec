//
//  CommonServices.swift
//  livec
//
//  Created by Santhosh on 11/08/19.
//  Copyright © 2019 Livec. All rights reserved.
//

import Foundation

class ServiceUtil {
    
    let session: URLSession = {
       let config = URLSessionConfiguration.default
       return URLSession(configuration: config)
    }()
    
    func getURL(url: String, parameters: [String:String]?) -> URL {
        var components = URLComponents(string: url)
        var queryItems = [URLQueryItem]()
        
        if let params = parameters {
            for (key, value) in params {
                let item = URLQueryItem(name: key, value: value)
                queryItems.append(item)
            }
        }
        components?.queryItems = queryItems
        return components!.url!
        
    }
    
}
