//
//  Connection.swift
//
//  Created by Santhosh on 07/08/19.
//  Copyright © 2019 Livec. All rights reserved.

import Foundation


class LoginService {
    let serviceUtil = ServiceUtil()
    
    func login (userName: String, password: String, viewController: ViewController) {
        let url = serviceUtil.getURL(url: URLConstants.loginURL,
                        parameters: ["userName": userName, "password": password])
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let task = serviceUtil.session.dataTask(with: request) {
            (data, response, error) -> Void in
            if let jsonData = data {
                if let json = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
                    print(json)
                    let homeVC = HomeViewController()
                    viewController.present(homeVC, animated: true, completion: nil)
                }
            }
            else if let requestError = error {
                print("Error Logging in \(requestError)")
            }
            else {
                print("Unexpected error with request")
            }
        }
        task.resume()
    }
}
