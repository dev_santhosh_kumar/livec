//
//  URLConstants.swift
//  livec
//
//  Created by Santhosh on 11/08/19.
//  Copyright © 2019 Livec. All rights reserved.
//

import Foundation

struct URLConstants {
    
    static let serverURL = "http://192.168.1.7:8080/"
    
    static let loginURL = serverURL+"login/"
}
