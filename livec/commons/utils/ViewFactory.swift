//
//  ViewFactory.swift
//  Provides convenience methods for generating
//  Views
//
//  Created by Santhosh on 28/07/19.
//  Copyright © 2019 Livec. All rights reserved.
//

import UIKit

class ViewFactory {
    static func getBaseView(container: UIView) -> BaseView {
        let baseView = BaseView(frame: CGRect(origin: container.frame.origin,size: container.frame.size))
        let titleView = getTitleView(container: baseView)
        let mainView = getMainView(container: baseView, titleView: titleView)
        
        baseView.setTitleView(titleView: titleView)
        baseView.setMainView(mainView: mainView)
        //Add constraints
        //baseView.topAnchor.constraint(equalTo: container.safeAreaLayoutGuide.topAnchor).isActive = true
        VuUtil.encloseInContainer(container: container, view: baseView)
        
        return baseView
    }
    
    static func getTitleView(container: BaseView) -> TitleView {
        let titleView = TitleView(frame: CGRect(origin: container.frame.origin, size: container.frame.size))
        titleView.backgroundColor = VuUtil.hexStringToUIColor(hex: ColorConstants.titleBackground)
        VuUtil.encloseInContainer(container: container, view: titleView, heightMultiplier: 0.1)
        return titleView
    }
    
    static func getMainView(container: BaseView, titleView: TitleView!) -> MainView {
        let mainView = MainView(frame: CGRect(origin: container.frame.origin, size: container.frame.size))
        mainView.backgroundColor = UIColor.darkGray
        container.addSubview(mainView)
        
        //Add constraints
        mainView.translatesAutoresizingMaskIntoConstraints = false
        if (titleView != nil) {
         mainView.topAnchor.constraint(equalTo: titleView.bottomAnchor).isActive = true
        } else {
         mainView.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
        }
        mainView.leadingAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
        mainView.widthAnchor.constraint(equalTo: container.widthAnchor).isActive = true
        mainView.heightAnchor.constraint(equalTo: container.heightAnchor).isActive = true
        
        return mainView
    }
    
    //Usage: view.layer.insertSublayer(ViewFactory.getDefaultGradientLayer(frame: CGRect), at: 0)
    static func getDefaultGradientLayer(frame: CGRect) -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.frame = frame
        gradient.colors = [VuUtil.hexStringToUIColor(hex: ColorConstants.commonGradientTop).cgColor, VuUtil.hexStringToUIColor(hex: ColorConstants.commonGradientBottom).cgColor]
        return gradient
    }
    
    //Usage: self.backgroundColor = ViewFactory.getImageBackground(frame,imageName)
    static func getImageBackground(frame: CGRect, imageName: String) -> UIColor {
        UIGraphicsBeginImageContext(frame.size)
        UIImage(named: imageName)?.draw(in: frame)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return UIColor(patternImage: image)
    }
    
    static func getButtonClickAnimation() -> CABasicAnimation {
        let colorAnimation = CABasicAnimation(keyPath: "backgroundColor")
        colorAnimation.fromValue = VuUtil.hexStringToUIColor(hex: ColorConstants.buttonPressAnimColor).cgColor
        colorAnimation.duration = 0.8
        return colorAnimation;
    }
    
}
