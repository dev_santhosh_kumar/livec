//
//  TextField.swift
//  Single destination to customize text field
//
//  Created by Santhosh on 31/07/19.
//  Copyright © 2019 Livec. All rights reserved.
//

import UIKit

class TextField: UITextField {
    
    init(frame: CGRect, placeHolder: String, keyboardType: UIKeyboardType) {
        super.init(frame: frame)
        
        self.backgroundColor = VuUtil.hexStringToUIColor(hex: ColorConstants.textFieldBackground)
        self.clearButtonMode = UITextField.ViewMode.whileEditing
        self.placeholder = placeHolder
        self.keyboardType = keyboardType
        
        self.layer.cornerRadius = 8.0
        self.layer.borderWidth = 2.0
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        super.textRect(forBounds: bounds)
        return bounds.insetBy(dx: 10, dy: 0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        super.editingRect(forBounds: bounds)
        return bounds.insetBy(dx: 10, dy: 0)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        super.placeholderRect(forBounds: bounds)
        return bounds.insetBy(dx: 10, dy: 0)
    }
    
    
}
