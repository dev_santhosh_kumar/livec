//
//  Button.swift
//  
//
//  Created by Santhosh on 02/08/19.
//  Copyright © 2019 Livec. All rights reserved.
//

import UIKit

class Button: UIButton {
    
    init(frame: CGRect, title: String) {
        super.init(frame: frame)
        self.setTitle(title, for: .normal)
        self.setTitleColor(VuUtil.hexStringToUIColor(hex: ColorConstants.buttonTitleColor), for: .normal)
        self.backgroundColor = VuUtil.hexStringToUIColor(hex: ColorConstants.buttonBackground)
        self.layer.cornerRadius = 8.0
        self.layer.borderWidth = 2.0
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
